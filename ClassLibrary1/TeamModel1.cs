﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary1
{
    /// <summary>
    /// reprezentuje jedną drużynę
    /// </summary>
    public class TeamModel1
    {
        /// <summary>
        /// reprezentuje członków drużyny 
        /// </summary>
        public List<PersonModel1> TeamMembers { get; set; } = new List<PersonModel1>();


        /// <summary>
        /// reprezentuje  nazwę drużyny
        /// </summary>
        public string TeamName { get; set; }
    }
}
