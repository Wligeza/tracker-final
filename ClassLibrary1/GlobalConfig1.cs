﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary1
{
    public static class GlobalConfig1
    {
        /// <summary>
        /// możliwość zapisania do bazy danych oraz do textu
        /// dlatego robimy listę połączeń
        /// ponieważ mamy więcej źródeł danych 
        /// </summary>
        public static List<IDataConnection1> Connections { get; private set; } = new List<IDataConnection1>(); 
        //każdy moze przeczytać wartość połączęń -> {get}
        //ale tylko w metodach można zmienić ich wartość -> {private set}

        public static void InitializeConnections(bool database, bool textFiles)     
        {
            if(database)
            {
                // TODO - set up SQL Connector properly
                SqlConnector1 sql = new SqlConnector1();
                Connections.Add(sql);
            }
            if(textFiles)
            {
                //TODO - set up TEXT Connector properly
                TextConnector1 text = new TextConnector1();
                Connections.Add(text);
            }
        }
    }
}
