﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary1
{
    /// <summary>
    ///  klasa stworzona do łącznia sie z SQL'em
    /// </summary>
    public class SqlConnector1 : IDataConnection1 // użycie f-cji: implementuj interfejs/("kontrakt") 
    {
        //TODO - zrób metode CreatePrize do zapisywania w bazie danych (database) 
        /// <summary>
        /// saves a new prize to the database 
        /// </summary>
        /// <param name="model"></param> informacje o nagrodzie 
        /// <returns> info o nagrodzie, wtym jej unikatowy id </returns> 
        public PrizeModel1 CreatePrize(PrizeModel1 model)
        {
            model.Id = 1;
            return model;
        }

       
    }
}
