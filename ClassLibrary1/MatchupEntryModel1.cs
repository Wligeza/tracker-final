﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary1
{
    /// <summary>
    /// reprezentuje jedną drużynę w meczu
    /// </summary>
    /// 
    public class MatchupEntryModel1
    {
        /// <summary>
        /// reprezentuje jedną drużynę w meczu
        /// </summary>
        public TeamModel1 TeamCompeting { get; set; }//


        /// <summary>
        /// reprezentuje wynik tej drużyny
        /// </summary>
        public double Score { get; set; }


        /// <summary>
        /// reprezentuje mecz z której ta drużyne przyszła jako zwycięzca
        /// </summary>
        public MatchupModel1 ParentMatchup { get; set; }

    }
}
