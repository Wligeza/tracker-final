﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Models

{
        /// <summary>
        /// reprezentuje jeden turniej
        /// </summary>
        public class TournamenModel
        {
            /// <summary>
            /// unkatowy identyfikator dla danej nagordy
            /// </summary>
            public int Id { get; set; }


            /// <summary>
            /// reprezentuje nazwa turnieju
            /// </summary>
            public string TournamentName { get; set; }


            /// <summary>
            /// reprezentuje opłata wstępna 
            /// </summary>
            public decimal EntryFee { get; set; }


            /// <summary>
            /// reprezentuje drużyny wchodzące
            /// </summary>
            public List<TeamModel> EnteredTeams { get; set; } = new List<TeamModel>();


            /// <summary>
            /// reprezentuje nagrody
            /// </summary>
            public List<PrizeModel> Prizes { get; set; } = new List<PrizeModel>();


            /// <summary>
            /// reprezentuje rundy
            /// </summary>
            public List<List<MatchupModel>> Rounds { get; set; } = new List<List<MatchupModel>>();
        }
}
