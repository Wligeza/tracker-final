﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Models
{
    /// <summary>
    /// reprezentuje jedną drużynę
    /// </summary>
    public class TeamModel
    {
        /// <summary>
        /// unkatowy identyfikator dla danej drużyny 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// reprezentuje  nazwę drużyny
        /// </summary>
        public string TeamName { get; set; }

        /// <summary>
        /// jest to lista zawierająca członków drużyny 
        /// </summary>
        public List<PersonModel> TeamMembers { get; set; } = new List<PersonModel>();


        
    }
}
