﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Models
{
    /// <summary>
    /// reprezentuje wygraną 
    /// </summary>
    public class PrizeModel
    {
        /// <summary>
        /// unkatowy identyfikator dla danej nagordy
        /// </summary>
        public int Id { get; set; }


        /// <summary>
        /// reprezentuje numer miejsca, np na podium
        /// </summary>
        public int PlaceNumber { get; set; }


        /// <summary>
        /// reprezentuje nazwę miejsca, np pierwsze miejsce na podium to czempion
        /// </summary>
        public string PlaceName { get; set; }


        /// <summary>
        /// reprezentuje stawkę nagrody
        /// </summary>
        public decimal PrizeAmount { get; set; }


        /// <summary>
        /// reprezentuje procent nagrody, np 2 miejsce dostaje 25% całej nagrody 
        /// </summary>
        public double PrizePercentage { get; set; }



        //konstruktor - pusty
        public PrizeModel()
        {

        }

        public PrizeModel(string placeName, string placeNumber, string prizeAmount, string prizePercentage)
        {
            ///PLACE NAME
            PlaceName = placeName;

            ///PLACE NUMBER
            int placeNumberValue = 0; //default
            int.TryParse(placeNumber, out placeNumberValue); //if false use default
            PlaceNumber = placeNumberValue;

            ///PRIZE AMOUNT
            decimal prizeAmountValue = 0; //default
            decimal.TryParse(prizeAmount, out prizeAmountValue); //if false use default
            PrizeAmount = prizeAmountValue;

            ///PRIZE PERCENTAGE
            double prizePercentageValue = 0; //default
            double.TryParse(prizePercentage, out prizePercentageValue); //if false use default
            PrizePercentage = prizePercentageValue;
        }

    }
}
