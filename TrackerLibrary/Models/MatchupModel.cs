﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Models
{
    /// <summary>
    /// reprezentuje jeden mecz w turnieju
    /// </summary>
    public class MatchupModel
    {
        /// <summary>
        /// unikatowy identyfikator dla danego matchup'u
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// zbiór drużyn które brały idział w meczu
        /// </summary>
        public List<MatchupEntryModel> Entries { get; set; } = new List<MatchupEntryModel>();

        /// <summary>
        /// the ID from database thath will be used to identify the winner.
        /// </summary>
        public int WinnerId { get; set; }
        
        /// <summary>
        /// reprezentuje zwycięzcę tego meczu
        /// </summary>
        public TeamModel Winner { get; set; }

        /// <summary>
        /// reprezentuje runde której ten mecz jest częścią 
        /// </summary>
        public int MatchupRound { get; set; }
    }
}
