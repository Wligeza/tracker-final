﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackerLibrary.Models
{
    /// <summary>
    ///  przechowuje infoarmacje o poprzedniej rozgrywce drużyny 
    ///  z której ta drużyne przyszła jako zwycięzca
    ///  oraz wynik tej drużyny 
    /// </summary>
    public class MatchupEntryModel
    {
        /// <summary>
        /// unikatowy identyfikator dla danego matchupEntryModel'u
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// unikatowy identyfikator dla danej drużyny 
        /// </summary>
        public int TeamCompetingId { get; set; }

        /// <summary>
        /// reprezentuje drużynę w MatchupEntryModel
        /// </summary>
        public TeamModel TeamCompeting { get; set; }


        /// <summary>
        /// reprezentuje wynik tej drużyny
        /// </summary>
        public double Score { get; set; }
        
        /// <summary>
        /// unikatowy identyfikator dla danego Parentmatchup
        /// </summary>
        public int ParentMatchupId { get; set; }

        /// <summary>
        /// reprezentuje mecz z której ta drużyne przyszła jako zwycięzca
        /// </summary>
        public MatchupModel ParentMatchup { get; set; }

    }
}
