﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.DataAccess;

namespace TrackerLibrary
{
    public static class GlobalConfig
    {

        public const string PrizesFile = "PrizeModels.csv";
        public const string PeopleFile = "PersonModels.csv";
        public const string TeamFile = "TeamModels.csv";
        public const string TournamentFile = "TournamentsModels.csv";
        public const string MatchupFile = " MatchupModels.csv";
        public const string MatchupEntryFile = " MatchupEntryModels.csv";

        public static IDataConnection Connection { get; private set; }     
        //każdy moze przeczytać wartość połączęń -> {get}
        //ale tylko w metodach można zmienić ich wartość -> {private set}



        public static void InitializeConnections(DatabaseType db)     
        {
            if(db == DatabaseType.Sql)
            {
                SqlConnector sql = new SqlConnector();
                Connection = sql;
            }
            else if(db == DatabaseType.TextFile)
            {
                TextConnector text = new TextConnector();
                Connection = text;
            }
        }

        /// <summary>
        /// SQL server shit
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static string CnnString(string name) //zajrzy do App.config, wyszuka Tournaments (jeśli to wpiszesz) i pobierze: Server=LAPT...sted_Connection = True
        {
            return ConfigurationManager.ConnectionStrings[name].ConnectionString;
        }
    }
}
