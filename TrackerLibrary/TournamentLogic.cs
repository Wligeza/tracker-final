﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Models;

namespace TrackerLibrary
{
    public static  class TournamentLogic
    {
        /// ranodomowo wybierasz drużyny 
        /// jeżeli ilosc drużyn jest nie wygodna - dodaj byes; potrzebujesz 2^n drużyn 
        /// stwórz pierwszą rundę matchupów
        /// stwórz resztę tych drużyn   
       


        public static void CreateRounds(TournamenModel model)
        {
            List<TeamModel> randomizedTeams = RandomizeTeamOrder(model.EnteredTeams); //pobieramy liste drużyn uczestniczących w turnieju i przesyłamy do funkcji lasującej
            int rounds = FindNumberOfRounds(randomizedTeams.Count); //określenie ilości rund 
            int byes = NumberOfByes(rounds, randomizedTeams.Count); //obliczanie ile potrzeba dodać dopełnień "byes" do 2^n miejsc gdzie n to rundy 
            model.Rounds.Add(CreateFirstRound(byes, randomizedTeams)); //tworzenie pierwszej rundy 
            CreateOtherRounds(model, rounds); //tworzenie reszty 
        }
                    

        private static void CreateOtherRounds(TournamenModel model, int rounds)
        {
            int round = 2; // bo już pierwszą mamy za soba 
            List<MatchupModel> previousRound = model.Rounds[0];       // model.Rounds[0] --> wybieramy pierwszą pozycję z tej listy 
            List<MatchupModel> currentRound = new List<MatchupModel>();
            MatchupModel currentMatchup = new MatchupModel();

            while(round <= rounds) //actualna: round, totalna: rounds 
            {
                foreach(MatchupModel match in previousRound)
                {
                    currentMatchup.Entries.Add(new MatchupEntryModel { ParentMatchup = match });

                    if(currentMatchup.Entries.Count > 1)
                    {
                        currentMatchup.MatchupRound = round;
                        currentRound.Add(currentMatchup);
                        currentMatchup = new MatchupModel();
                    }
                }
                model.Rounds.Add(currentRound);
                previousRound = currentRound; 
                currentRound = new List<MatchupModel>();
                round += 1;
            }
        }

        private static List<MatchupModel> CreateFirstRound(int byes, List<TeamModel> teams)
        {
            List<MatchupModel> output = new List<MatchupModel>();
            MatchupModel current = new MatchupModel();

            foreach(TeamModel team in teams) 
            {
                current.Entries.Add(new MatchupEntryModel { TeamCompeting = team });

                if(byes > 0 || current.Entries.Count > 1)
                {
                    current.MatchupRound = 1; //jak nazwa mówi.. pierwsza runda 
                    output.Add(current); 
                    current = new MatchupModel(); //"zerujemy"

                    if(byes > 0 )
                    {
                        byes -= 1;
                    }
                }
            }

            return output;
        }

        private static int NumberOfByes(int rounds, int numberOfTeams)
        {
            int output = 0;
            int totalTeams = 1;

            for(int i = 1; i <= rounds; i++)
            {
                totalTeams *= 2;
            }

            output = totalTeams - numberOfTeams;
            return output;

        }

        private static int FindNumberOfRounds(int teamCount)
        {
            int output = 1;
            int val = 2;

            while (val < teamCount)
            {
                
                output += 1;
                val *= 2;
            }

            return output; 

        }


        private static List<TeamModel> RandomizeTeamOrder(List<TeamModel> teams)
        {
           //cards.OrderBy(async => Guid.NewGuid()).ToList();  //----> random system template 
            return teams.OrderBy(x => Guid.NewGuid()).ToList();
        }
    }
}
