﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Models;
using TrackerLibrary.DataAccess.TextHelpers;

namespace TrackerLibrary.DataAccess
{
    /// <summary>
    /// klasa stworzona do łącznia sie z plikiem .txt
    /// </summary>
    public class TextConnector : IDataConnection
    {
        private const string PrizesFile = "PrizeModels.csv";
        private const string PeopleFile = "PersonModels.csv";
        private const string TeamFile = "TeamModels.csv";
        private const string TournamentFile = "TournamentsModels.csv";
        private const string MatchupFile = " MatchupModels.csv";
        private const string MatchupEntryFile = " MatchupEntryModels.csv";



        //---------------------------------------------------------------------------------------------------------------CREATE
        /// <summary>
        /// saves a new PERSON to the file
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public PersonModel CreatePerson(PersonModel model)
        {
            List<PersonModel> people = PeopleFile.FullFilePath().LoadFile().ConvertToPersonModels();
            int currentId = 1;

            if(people.Count > 0)
            {
                currentId = people.OrderByDescending(x => x.Id).First().Id + 1;
            }
            model.Id = currentId;

            people.Add(model);

            people.SaveToPeopleFile(PeopleFile);

                return model;
        }


        /// <summary>
        /// saves a new PRIZE to the file
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public PrizeModel CreatePrize(PrizeModel model)
        {
            // * Load the txt file        
            // * Convert the text to List<PrizeModel>
            List<PrizeModel> prizes = PrizesFile.FullFilePath().LoadFile().ConvertToPrizeModels(); //lista: prize (PrizeModel)

            // * find the max ID
            int currentId = 1; 

            if(prizes.Count > 0)
            {
                     currentId = prizes.OrderByDescending(x => x.Id).First().Id + 1;  
            }

            model.Id = currentId;

            // * Add the new record with the new ID (max + 1)
            prizes.Add(model);

            
            // * save the List<string> to the text file
            prizes.SaveToPrizeFile(PrizesFile);

            return model;
        }


        /// <summary>
        /// saves a new TEAM to the file
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public TeamModel CreateTeam(TeamModel model)
        {
            List<TeamModel> teams = TeamFile.FullFilePath().LoadFile().ConvertToTeamModels(PeopleFile);

            int currentId = 1;

            if ( teams.Count > 0)
            {
                currentId = teams.OrderByDescending(x => x.Id).First().Id + 1;
            }
            model.Id = currentId;

            teams.Add(model);

            teams.SaveToTeamFile(TeamFile);

            return model;

        }

        /// <summary>
        /// saves a new TOURNAMENT to the file
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public void CreateTournament(TournamenModel model)
        {
            List<TournamenModel> tournaments = TournamentFile
                .FullFilePath()
                .LoadFile()
                .ConvertToTournamentsModels(TeamFile, PeopleFile, PrizesFile);

            int currentId = 1;

            if (tournaments.Count > 0)
            {
                currentId = tournaments.OrderByDescending(x => x.Id).First().Id + 1;
            }

            model.Id = currentId;

            model.SaveRoundsToFile( MatchupFile, MatchupEntryFile);

            tournaments.Add(model);

            tournaments.SaveToTournamentFile(TournamentFile);
        }










        //---------------------------------------------------------------------------------------------------------------GET
        /// <summary>
        /// takes people's info from FILE
        /// </summary>
        /// <returns></returns>
        public List<PersonModel> GetPerson_All()
        {
            return PeopleFile.FullFilePath().LoadFile().ConvertToPersonModels();
        }

        /// <summary>
        /// takes team's info from FILE
        /// </summary>
        /// <returns></returns>
        public List<TeamModel> GetTeam_All()
        {
            return TeamFile.FullFilePath().LoadFile().ConvertToTeamModels(PeopleFile);
        }

        public List<TournamenModel> GetTournament_All()
        {
           return TournamentFile.FullFilePath().LoadFile().ConvertToTournamentsModels(TeamFile, PeopleFile, PrizesFile);
        }
    }
}
