﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Models;

// tu wukonywane są te kroki (z klasy TextConnector):
// * Load the txt file        
// * Convert the text to List<PrizeModel>
// * find the max ID
// * Add the new record with the new ID (max + 1)
// * convert the prize to List<string>
// * save the List<string> to the text file

namespace TrackerLibrary.DataAccess.TextHelpers
{
    public static class TextConnectorProcessor
    {

        public static string FullFilePath(this string fileName) ///path eg: C:\data\TournamentTracker\PrizeModels.csv
        {
            return $"{ ConfigurationManager.AppSettings["filePath"] }\\{ fileName }"; // $ -> skleja stringi
            ///w App.config masz <appSettings> do czego sie ta funckcja odwołuje, a szukany key znajduje się w []
            ///a fukncja powinna ret for eg: "C:\data\TournamentTracker"
        }

        /// <summary>
        /// Loads txt files 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static List<string> LoadFile(this string file)
        {
            if (!File.Exists(file)) //systemowa funkcja bool 
            {
                return new List<string>();
            }

            return File.ReadAllLines(file).ToList();
        }








        ///
        ///-------------------------------------------------------------------------------------------------------------LOADING SECTION 
        ///

        //----------------------------------------------------------------------------------- Convert To FULL Models
        // * Convert the text to List<PrizeModel>

        /// <summary>
        /// Converts the text to List<PrizeModel>
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        public static List<PrizeModel> ConvertToPrizeModels(this List<string> lines)
        {
            List<PrizeModel> output = new List<PrizeModel>(); ///tworzymy nową listę stringów -> output 

            foreach (string line in lines)
            {
                string[] cols = line.Split(',');    //cols - columns 

                PrizeModel p = new PrizeModel();

                p.Id = int.Parse(cols[0]);
                p.PlaceNumber = int.Parse(cols[1]);
                p.PlaceName = cols[2];
                p.PrizeAmount = decimal.Parse(cols[3]);
                p.PrizePercentage = double.Parse(cols[4]);
                output.Add(p);
            }
            return output;
        }

        /// <summary>
        /// Converts the text to List<PersonModel>
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        public static List<PersonModel> ConvertToPersonModels(this List<string> lines)
        {
            List<PersonModel> output = new List<PersonModel>();
            {
                foreach (string line in lines)
                {
                    string[] cols = line.Split(',');

                    PersonModel p = new PersonModel();

                    p.Id = int.Parse(cols[0]);
                    p.FirstName = cols[1];
                    p.LastName = cols[2];
                    p.EmailAddress = cols[3];
                    p.CellphoneNumber = cols[4];
                    output.Add(p);
                }
                return output;
            }
        }

        /// <summary>
        /// Converts the text to List<TeamModel>
        /// </summary>
        /// <param name="lines"></param>
        /// <param name="peopleFileName"></param>
        /// <returns></returns>
        public static List<TeamModel> ConvertToTeamModels(this List<string> lines, string peopleFileName)
        {
            //Id,TeamName, (list of team members ids separated by the pipe)
            //eg: 3,Sandecja, 2|1|3|7

            List<TeamModel> output = new List<TeamModel>();
            List<PersonModel> people = peopleFileName.FullFilePath().LoadFile().ConvertToPersonModels(); //pobiera listę ludzi

            foreach (string line in lines)
            {
                string[] cols = line.Split(',');

                TeamModel t = new TeamModel();
                t.Id = int.Parse(cols[0]);
                t.TeamName = cols[1];

                string[] personIds = cols[2].Split('|');
                foreach (string id in personIds)
                {
                    t.TeamMembers.Add(people.Where(x => x.Id == int.Parse(id)).First());
                }
                output.Add(t);
            }
            return output;
        }

        /// <summary>
        /// Converts the text to List<TournamentModel>
        /// </summary>
        /// <returns></returns>
        public static List<TournamenModel> ConvertToTournamentsModels(
            this List<string> lines,
            string teamFileName,
            string poepleFileName,
            string prizesFileName)
        {
            // id = 0
            // TournamentName = 1
            // EntryFee = 2
            // EnteredTeams = 3 
            // Prizes = 4 
            // Rounds = 5
            // Id,TournamentName,EntryFee, (list of entered teams ids separated by the pipe), (list of entered prizes ids separated by the pipe), (Rounds- id^id^id|id^id^id|id^id^id)
            // eg: 3,liga czempionuuf, 2|1|3|7, 3|4|2|1, 1^3^8|12^4^5|7^3^2  -> troche pojebane 

            List<TournamenModel> output = new List<TournamenModel>();
            List<TeamModel> teams = teamFileName.FullFilePath().LoadFile().ConvertToTeamModels(poepleFileName);
            List<PrizeModel> prizes = prizesFileName.FullFilePath().LoadFile().ConvertToPrizeModels();
            List<MatchupModel> matchups = GlobalConfig.MatchupFile.FullFilePath().LoadFile().ConvertToMatchupModels();

            foreach (string line in lines)
            {
                string[] cols = line.Split(',');

                TournamenModel tm = new TournamenModel();
                tm.Id = int.Parse(cols[0]);
                tm.TournamentName = cols[1];
                tm.EntryFee = decimal.Parse(cols[2]);

                string[] teamId = cols[3].Split('|');
                foreach (string id in teamId)
                {
                    tm.EnteredTeams.Add(teams.Where(x => x.Id == int.Parse(id)).First());
                }

                if (cols[4].Length > 0)
                {
                    string[] prizeId = cols[4].Split('|');
                    foreach (string id in prizeId)
                    {
                        tm.Prizes.Add(prizes.Where(x => x.Id == int.Parse(id)).First());
                    }
                }

                //Capture Rounds Infos
                //list of list of MatchupModels
                string[] rounds = cols[5].Split('|');
                

                foreach(string round in rounds)
                {
                    string[] msText = round.Split('^');
                    List<MatchupModel> ms = new List<MatchupModel>();

                    foreach (string matchupModelTextId in msText)
                    {
                        ms.Add(matchups.Where(x => x.Id == int.Parse(matchupModelTextId)).First());
                       
                    }

                    tm.Rounds.Add(ms);
                }

                output.Add(tm);
            }


            return output;
        }


        /// <summary>
        /// Converts the text to List<Matchup>
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        public static List<MatchupModel> ConvertToMatchupModels(this List<string> lines)
        {
            //id = 0, entries=1(pipe delimited by id), winner = 2, matchupRound = 3
            List<MatchupModel> output = new List<MatchupModel>();
            {
                foreach (string line in lines)
                {
                    string[] cols = line.Split(',');

                    MatchupModel m = new MatchupModel();
                    
                    m.Id = int.Parse(cols[0]);
                    m.Entries = ConvertStringToMatchupEntryModels(cols[1]); // więc mamy stringa i musimy zamienić go na listę matchupmodel
                    if(cols[2].Length == 0)
                    {
                        m.Winner = null;
                    }
                    else
                    {
                        m.Winner = LookupTeamById(int.Parse(cols[2]));
                    }
                    
                    m.MatchupRound = int.Parse(cols[3]);
                    output.Add(m);
                }
                return output;
            }
        }


        /// <summary>
        /// Converts the text to a List<MatchupEntryModel>
        /// </summary>
        /// <param name="lines"></param>
        /// <returns></returns>
        public static List<MatchupEntryModel> ConvertToMatchupEntryModels(this List<string> lines)
        {
            //id = 0, TeamCompeting (id) = 1, Score = 2, ParentMatchup(id) = 3
            List<MatchupEntryModel> output = new List<MatchupEntryModel>();
            {
                foreach (string line in lines)
                {
                    string[] cols = line.Split(',');

                    MatchupEntryModel me = new MatchupEntryModel();

                    me.Id = int.Parse(cols[0]);


                    if (cols[1].Length == 0)
                    {
                        me.TeamCompeting = null;
                    }
                    else
                    {
                        me.TeamCompeting = LookupTeamById(int.Parse(cols[1]));
                    }
                    
                    me.Score = double.Parse(cols[2]);

                    int parentId = 0;
                    if (int.TryParse(cols[3], out parentId))
                    {
                        me.ParentMatchup = LookupMatchupById(parentId);
                    }
                    else
                    {
                        me.ParentMatchup = null;
                    }


                    output.Add(me);
                }
                return output;
            }
        }



        //----------------------------------------------------------------------------------- Helpers
        /// <summary>
        /// funkcja powiązana z funkcją: ConvertToMatchupModels
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static List<MatchupEntryModel> ConvertStringToMatchupEntryModels(string input)
        {
            string[] ids = input.Split('|');
            List<MatchupEntryModel> output = new List<MatchupEntryModel>();
            List<string> entries = GlobalConfig.MatchupEntryFile.FullFilePath().LoadFile();
            List<string> matchingEntries = new List<string>();

            foreach (string id in ids)
            {
                foreach(string entry in entries)
                {
                    string[] cols = entry.Split(',');
                    if (cols[0] == id)
                    {
                        matchingEntries.Add(entry);
                    }
                }
            }
            output = matchingEntries.ConvertToMatchupEntryModels();

            return output;
        }

        /// <summary>
        /// funkcja powiązana z funkcją: ConvertToMatchupModels
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static TeamModel LookupTeamById(int id)
        {
            List<string> teams = GlobalConfig.TeamFile.FullFilePath().LoadFile();

            foreach(string team in teams)
            {
                string[] cols = team.Split(',');
                if(cols[0] == id.ToString())
                {
                    List<string> matchingTeams = new List<string>();
                    matchingTeams.Add(team);
                    return matchingTeams.ConvertToTeamModels(GlobalConfig.PeopleFile).First();
                }
            }
            return null;
        }
        
        /// <summary>
        /// funkcja powiązana z funkcją: ConvertToMatchuEntrypModels
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static MatchupModel LookupMatchupById(int id)
        {
            List<string> matchups = GlobalConfig.MatchupFile.FullFilePath().LoadFile();
            
            foreach (string matchup in matchups)
            {
                string[] cols = matchup.Split(',');
                if (cols[0] == id.ToString())
                {
                    List<string> matchingMatchups = new List<string>();
                    matchingMatchups.Add(matchup);
                    return matchingMatchups.ConvertToMatchupModels().First();
                }
            }
            return null;
        }

      








        // TournamentModel contains ROUNDS which is list of a list of MatchupModels
        // ROUNDS: 1^3^8 | 12^4^5 |7^3^2 , each object seperated by |
        // list of MatchupModels: 1^3^8 , each object seperated by ^  //THIS IS SINGLE ROUND OF ROUNDS LIST
        // MatchupModel: 1 
        // each MatchupModel contains another list: ENTRY
        // ENTRY is a list of MatchupEntryModel which contains info about: TeamCompeting, Score, ParentMatchup. Also has its own Id 

            
        ///
        ///-------------------------------------------------------------------------------------------------------------SAVING SECTION 
        ///




        //----------------------------------------------------------------------------------- Convert TO string SAVING
        //team
        /// <summary>
        /// funkcja powiązana z SaveToTeamFile
        /// </summary>
        /// <param name="people"></param>
        /// <returns></returns>
        private static string ConvertPeopleListToString(List<PersonModel> people)
        {
            string output = "";

            if (people.Count == 0)
            {
                return "";
            }

            foreach (PersonModel p in people)
            {
                output += $"{p.Id}|";
            }

            output = output.Substring(0, output.Length - 1);

            return output;
        }


        //tournament
        /// <summary>
        /// funkcja powiązana z SaveToTournamentFile
        /// </summary>
        /// <param name="teams"></param>
        /// <returns></returns>
        public static string ConvertTeamsListToString(List<TeamModel> teams)
        {
            string output = "";

            if (teams.Count == 0)
            {
                return "";
            }

            foreach (TeamModel t in teams)
            {
                output += $"{t.Id}|";
            }

            output = output.Substring(0, output.Length - 1);


            return output;
        }

        /// <summary>
        /// funkcja powiązana z SaveToTournamentFile
        /// </summary>
        /// <param name="prizes"></param>
        /// <returns></returns>
        public static string ConvertPrizesListToString(List<PrizeModel> prizes)
        {
            string output = "";

            if (prizes.Count == 0)
            {
                return "";
            }

            foreach (PrizeModel p in prizes)
            {
                output += $"{p.Id}|";
            }

            output = output.Substring(0, output.Length - 1);


            return output;
        }

        /// <summary>
        /// funkcja powiązana z SaveToTournamentFile
        /// </summary>
        /// <param name="rounds"></param>
        /// <returns></returns>
        public static string ConvertRoundListToString(List<List<MatchupModel>> rounds)
        {
            // Rounds - id ^ id ^ id | id ^ id ^ id | id ^ id ^ id

            string output = "";

            if (rounds.Count == 0)
            {
                return "";
            }

            foreach (List<MatchupModel> r in rounds)
            {
                output += $"{ConvertMatchupListToString(r)}|";
            }

            output = output.Substring(0, output.Length - 1);


            return output;
        }

        /// <summary>
        /// funkcja powiązana z funckją ConvertRoundListToString ktr jest powiązana z SaveToTournamentFile // THIS IS EACH ROUND OF ROUNDS LIST
        /// </summary>
        /// <param name="matchups"></param>
        /// <returns></returns>
        private static string ConvertMatchupListToString(List<MatchupModel> matchups)
        {
            // MatchupEntryModel - id ^ id ^ id
            string output = "";

            if (matchups.Count == 0)
            {
                return "";
            }

            foreach (MatchupModel m in matchups)
            {
                output += $"{m.Id}^";
            }

            output = output.Substring(0, output.Length - 1);

            return output;
        }

        /// <summary>
        /// funkcja powiązana z SaveToTournamentFile
        /// </summary>
        /// <param name="prizes"></param>
        /// <returns></returns>
        public static string ConvertMatchupEntryListToString(List<MatchupEntryModel> entries)
        {
            string output = "";

            if (entries.Count == 0)
            {
                return "";
            }

            foreach (MatchupEntryModel e in entries)
            {
                output += $"{e.Id}|";
            }

            output = output.Substring(0, output.Length - 1);


            return output;
        }


        //----------------------------------------------------------------------------------- Save To Files 


        /// <summary>
        /// zapisuje do pliku: PeopleModels.csv
        /// </summary>
        /// <param name="models"></param>
        /// <param name="fileName"></param>
        public static void SaveToPeopleFile(this List<PersonModel> models, string fileName)
        {
            List<string> lines = new List<string>();

            foreach (PersonModel p in models)
            {
                lines.Add($"{ p.Id },{ p.FirstName },{ p.LastName },{ p.EmailAddress },{ p.CellphoneNumber}");
            }

            File.WriteAllLines(fileName.FullFilePath(), lines);

        }
        
        /// <summary>
        /// zapisuje do pliku: PrizeModels.csv
        /// </summary>
        /// <param name="models"></param>
        /// <param name="fileName"></param>
        public static void SaveToPrizeFile(this List<PrizeModel> models, string fileName)
        {
            // * convert the prize to List<string>
            List<string> lines = new List<string>();

            foreach (PrizeModel p in models)
            {
                lines.Add($"{ p.Id },{ p.PlaceNumber },{ p.PlaceName },{ p.PrizeAmount },{ p.PrizePercentage }");
            }

            // * save the List<string> to the text file
            File.WriteAllLines(fileName.FullFilePath(), lines); ///musimy podać ścieżkę do pliku oraz jego nazwe oraz liste naszych stringow do wypisania 
        }



        //team 
        /// <summary>
        /// zapisuje do pliku: TeamModels.csv
        /// </summary>
        /// <param name="models"></param>
        /// <param name="fileName"></param>
        public static void SaveToTeamFile(this List<TeamModel> models, string fileName)
        {

            List<string> lines = new List<string>();

            foreach (TeamModel t in models)
            {
                lines.Add($"{t.Id},{t.TeamName},{ConvertPeopleListToString(t.TeamMembers)}");
            }

            File.WriteAllLines(fileName.FullFilePath(), lines);
        }

       
        
        //tournament 
        /// <summary>
        /// zapisuje do pliku: TournamentsModels.csv
        /// </summary>
        /// <param name="models"></param>
        /// <param name="fileName"></param>
        public static void SaveToTournamentFile(this List<TournamenModel> models, string fileName)
        {
            List<string> lines = new List<string>();

            foreach (TournamenModel tm in models)
            {
                lines.Add($@"{tm.Id},{tm.TournamentName},{tm.EntryFee},{ConvertTeamsListToString(tm.EnteredTeams)},{ConvertPrizesListToString(tm.Prizes)},{ConvertRoundListToString(tm.Rounds)}");
            }
            File.WriteAllLines(fileName.FullFilePath(), lines);
        }
        
        /// <summary>
        /// funckja zapiosuje rundy do pliku --> powiązana z tworzeniem turnieju 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="MatchupFile"></param>
        /// <param name="MatchupEntryFile"></param>
        public static void SaveRoundsToFile(this TournamenModel model, string MatchupFile, string MatchupEntryFile)        //---> troche skomplikowane ;D
        {
            // loop przez listę Rounds
            // loop przez listę round (matchup'ów) (czyli pojedyńczy obiekt round w liście rund (Rounds) --> lista w liście 
            // pobierz id z kazdego matchupu i zapisz
            // loop prez każde Entry oraz pobierz i zapisz id 

            foreach (List<MatchupModel> round in model.Rounds)
            {
                foreach (MatchupModel matchup in round)
                {
                    // load all of the matchups form file 
                    // get the to id and add one 
                    // store the id 
                    // svae the matchup record 

                    matchup.SaveMatchupToFile();
                }
            }

        }



        //Matchup (połączone z Tournament)
        /// <summary>
        /// funkcja powiązana z funkcją:  SaveRoundsToFile
        /// </summary>
        /// <param name="matchup"></param>
        /// <param name="matchupFile"></param>
        public static void SaveMatchupToFile(this MatchupModel matchup)
        {
            //id = 0, entries=1(pipe delimited by id), winner = 2, matchupRound = 3

            List<MatchupModel> matchups = GlobalConfig.MatchupFile.FullFilePath().LoadFile().ConvertToMatchupModels();

            int currentId = 1;

            if (matchups.Count > 0)
            {
                currentId = matchups.OrderByDescending(x => x.Id).First().Id + 1;
            }

            matchup.Id = currentId;

            matchups.Add(matchup);

            foreach (MatchupEntryModel entry in matchup.Entries)
            {
                entry.SaveEntryToFile();
            }

            
            //save to file
            List<string> lines = new List<string>();

            foreach (MatchupModel m in matchups)
            {
                string winner = "";
                if (m.Winner != null)
                {
                    winner = m.Winner.Id.ToString();
                }

                lines.Add($"{m.Id},{ ConvertMatchupEntryListToString(m.Entries)},{winner},{m.MatchupRound}");
            }
            File.WriteAllLines(GlobalConfig.MatchupFile.FullFilePath(), lines);
        }

        /// <summary>
        /// funkcja powiązana z funkcją:  SaveMatchupToFile która jest częścią funkcji SaveRoundsToFile
        /// </summary>
        /// <param name="entry"></param>
        /// <param name="MatchupEntryFile"></param>
        public static void SaveEntryToFile(this MatchupEntryModel entry)
        {

            //id = 0, TeamCompeting (id) = 1, Score = 2, ParentMatchup(id) = 3
            List<MatchupEntryModel> entries = GlobalConfig.MatchupEntryFile.FullFilePath().LoadFile().ConvertToMatchupEntryModels();

            int currentId = 1;

            if (entries.Count > 0)
            {
                currentId = entries.OrderByDescending(x => x.Id).First().Id + 1;
            }

            entry.Id = currentId;
            entries.Add(entry);

            List<string> lines = new List<string>();
            foreach( MatchupEntryModel e in entries )
            {
                string parent = "";
                if (e.ParentMatchup != null)
                {
                    parent = e.ParentMatchup.Id.ToString();
                }
                string teamCompeting = "";
                if(e.TeamCompeting != null)
                {
                    teamCompeting = e.TeamCompeting.Id.ToString();
                }

                lines.Add($"{e.Id},{teamCompeting },{e.Score},{parent}");
            }
            File.WriteAllLines(GlobalConfig.MatchupEntryFile.FullFilePath(), lines);
        }

      


    }
}
