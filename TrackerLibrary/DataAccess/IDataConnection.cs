﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Models;

namespace TrackerLibrary.DataAccess
{
    /// <summary>
    ///----------------------------------------------------------- INTERFEJS 
    /// </summary>
    public interface IDataConnection
    {
        //prize
        PrizeModel CreatePrize(PrizeModel model);

        //person
        PersonModel CreatePerson(PersonModel model);
        List<PersonModel> GetPerson_All();

        //team
        TeamModel CreateTeam(TeamModel model);
        List<TeamModel> GetTeam_All();

        //tournament
        void CreateTournament(TournamenModel model);
        List<TournamenModel> GetTournament_All();
        
    }
}
