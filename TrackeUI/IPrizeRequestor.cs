﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrackerLibrary.Models;

namespace TrackeUI
{
    public interface IPrizeRequestor
    {
        /// <summary>
        /// dostajemy model 
        /// </summary>
        /// <param name="model"></param>
        void PrizeComplete(PrizeModel model);
    }
}
