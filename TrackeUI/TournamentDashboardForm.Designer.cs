﻿namespace TrackeUI
{
    partial class TournamentDashboardForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.HeaderLabel = new System.Windows.Forms.Label();
            this.loadExistingTournamentDropDown = new System.Windows.Forms.ComboBox();
            this.loadExistingTournamentlabel = new System.Windows.Forms.Label();
            this.loadTournamentButton = new System.Windows.Forms.Button();
            this.createTournamenButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // HeaderLabel
            // 
            this.HeaderLabel.AutoSize = true;
            this.HeaderLabel.Font = new System.Drawing.Font("Segoe UI Light", 27.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.HeaderLabel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.HeaderLabel.Location = new System.Drawing.Point(78, 24);
            this.HeaderLabel.Name = "HeaderLabel";
            this.HeaderLabel.Size = new System.Drawing.Size(386, 50);
            this.HeaderLabel.TabIndex = 13;
            this.HeaderLabel.Text = "Tournament Dashboard";
            // 
            // loadExistingTournamentDropDown
            // 
            this.loadExistingTournamentDropDown.FormattingEnabled = true;
            this.loadExistingTournamentDropDown.Location = new System.Drawing.Point(87, 122);
            this.loadExistingTournamentDropDown.Name = "loadExistingTournamentDropDown";
            this.loadExistingTournamentDropDown.Size = new System.Drawing.Size(377, 38);
            this.loadExistingTournamentDropDown.TabIndex = 21;
            // 
            // loadExistingTournamentlabel
            // 
            this.loadExistingTournamentlabel.AutoSize = true;
            this.loadExistingTournamentlabel.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loadExistingTournamentlabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.loadExistingTournamentlabel.Location = new System.Drawing.Point(138, 87);
            this.loadExistingTournamentlabel.Name = "loadExistingTournamentlabel";
            this.loadExistingTournamentlabel.Size = new System.Drawing.Size(290, 32);
            this.loadExistingTournamentlabel.TabIndex = 20;
            this.loadExistingTournamentlabel.Text = "Load Existing Tournament";
            // 
            // loadTournamentButton
            // 
            this.loadTournamentButton.BackColor = System.Drawing.SystemColors.HotTrack;
            this.loadTournamentButton.FlatAppearance.BorderColor = System.Drawing.Color.Navy;
            this.loadTournamentButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.loadTournamentButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.loadTournamentButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loadTournamentButton.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.loadTournamentButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.loadTournamentButton.Location = new System.Drawing.Point(144, 166);
            this.loadTournamentButton.Name = "loadTournamentButton";
            this.loadTournamentButton.Size = new System.Drawing.Size(277, 53);
            this.loadTournamentButton.TabIndex = 28;
            this.loadTournamentButton.Text = "Load Tournament";
            this.loadTournamentButton.UseVisualStyleBackColor = false;
            // 
            // createTournamenButton
            // 
            this.createTournamenButton.BackColor = System.Drawing.SystemColors.HotTrack;
            this.createTournamenButton.FlatAppearance.BorderColor = System.Drawing.Color.Navy;
            this.createTournamenButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Yellow;
            this.createTournamenButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.createTournamenButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.createTournamenButton.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.createTournamenButton.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.createTournamenButton.Location = new System.Drawing.Point(67, 225);
            this.createTournamenButton.Name = "createTournamenButton";
            this.createTournamenButton.Size = new System.Drawing.Size(417, 95);
            this.createTournamenButton.TabIndex = 29;
            this.createTournamenButton.Text = "Create Tournament";
            this.createTournamenButton.UseVisualStyleBackColor = false;
            // 
            // TournamentDashboardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 30F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.ClientSize = new System.Drawing.Size(556, 361);
            this.Controls.Add(this.createTournamenButton);
            this.Controls.Add(this.loadTournamentButton);
            this.Controls.Add(this.loadExistingTournamentDropDown);
            this.Controls.Add(this.loadExistingTournamentlabel);
            this.Controls.Add(this.HeaderLabel);
            this.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.Name = "TournamentDashboardForm";
            this.Text = "TournamentDashboardForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label HeaderLabel;
        private System.Windows.Forms.ComboBox loadExistingTournamentDropDown;
        private System.Windows.Forms.Label loadExistingTournamentlabel;
        private System.Windows.Forms.Button loadTournamentButton;
        private System.Windows.Forms.Button createTournamenButton;
    }
}