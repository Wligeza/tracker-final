﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;
using TrackerLibrary.DataAccess;
using TrackerLibrary.Models;

namespace TrackeUI
{
    public partial class CreatePrizeForm : Form
    {
        IPrizeRequestor callingForm;

        public CreatePrizeForm(IPrizeRequestor caller)
        {
            InitializeComponent();

            callingForm = caller;
        }

        private void createPrizeButton_Click(object sender, EventArgs e)
        {
            if(ValidateForm())
            {
               PrizeModel model = new PrizeModel(
                    placeNameValue.Text, 
                    placeNumberValue.Text,
                    prizeAmountValue.Text,
                    prizePercentageValue.Text);



                GlobalConfig.Connection.CreatePrize(model);

                callingForm.PrizeComplete(model);

                this.Close();

                //placeNameValue.Text = "";
                //placeNumberValue.Text = "";
                //prizeAmountValue.Text = "0";
                //prizePercentageValue.Text = "0";
                //MessageBox.Show("THE PRIZE WAS ADDED SUCCESSFULLY ;D");
            }
            else
            {
                MessageBox.Show("WRONG INFORMATION, TRY AGAIN!!!");
            }
        }

        

        /// <summary>
        /// przypadki
        /// </summary>
        /// <returns> output is bool -> (true/false) </returns>
        private bool ValidateForm()  
        {
            bool output = true;
            int placeNumber = 0;
            bool placeNumberValidNumber = int.TryParse(placeNumberValue.Text, out placeNumber);
            // cin >> string(z tabelki Place Number) -> convert to int -> out: int placeNumber
            // if TryParse nie zadziała zwróci FALSE, jeżeli zadziała zwróci TRUE
            // np jeżeli ktoś wpisze litere zamiast inta conversja nie zostanie wykonana zwróci FALSE 

            ///PLACE NUMBER
            if (placeNumberValidNumber == false) //jezeli nr jest niewłaściwy, np: a 
            {
                output = false;
            }

            if (placeNumber < 1) //jezeli nr jest niewłaściwy, np: -1 
            {
                output = false;
            }

            ///PLACE NAME
            if (placeNameValue.Text.Length == 0) //jezeli nie wpisano nic do pola Place Name 
            {
                output = false;
            }

            ///PRIZE AMOUNT
            decimal prizeAmount = 0;
            double prizePercentage = 0;

            bool prizeAmountValid = decimal.TryParse(prizeAmountValue.Text, out prizeAmount);
            bool prizePercentageValid = double.TryParse(prizePercentageValue.Text, out prizePercentage);


            if (prizeAmountValid == false || prizePercentageValid == false) ////jezeli input jest niewłaściwy, np: a 
            {
                output = false;
            }

            if (prizeAmount <= 0 && prizePercentage <= 0)  //jezeli input jest niewłaściwy, np: -1 
            {
                output = false;
            }

            if (prizePercentage > 100 || prizePercentage < 0) //bramki procentowe 
            {
                output = false;
            }


            return output;
        }

        private void priceAmountValue_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
