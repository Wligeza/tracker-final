﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;
using TrackerLibrary.Models;

namespace TrackeUI
{
    public partial class TournamentDashboardForm : Form
    {
        List<TournamenModel> tournamente = GlobalConfig.Connection.GetTournament_All();

        public TournamentDashboardForm()
        {
            InitializeComponent();

            WireupLists();
        }

        private void WireupLists()
        {
               loadExistingTournamentDropDown.DataSource = tournamente;
               loadExistingTournamentDropDown.DisplayMember = "TournamentName";

        }
    }
}
