﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;
using TrackerLibrary.Models;

namespace TrackeUI
{
    public partial class CreateTeamForm : Form
    {
        private List<PersonModel> avilableTeamMembers = GlobalConfig.Connection.GetPerson_All(); //łączy sie z bazą danych i pobiera dostępne info 
        private List<PersonModel> selectedTeamMembers = new List<PersonModel>(); //zapisuje do dropBoxa 

        private ITeamRequestor callingForm;
        public CreateTeamForm(ITeamRequestor caller)
        {
            InitializeComponent();

            callingForm = caller;

            WireUpLists();
        }

     

        public void WireUpLists()
        {
            //drop down
            selectTeamMemberDropDown.DataSource = null;
            selectTeamMemberDropDown.DataSource = avilableTeamMembers;
            selectTeamMemberDropDown.DisplayMember = "FullName";

            //list box
            teamMembersListBox.DataSource = null;
            teamMembersListBox.DataSource = selectedTeamMembers;
            teamMembersListBox.DisplayMember = "FullName";
        }





        private void firstNameValue_TextChanged(object sender, EventArgs e)
        {

        }

        private void createMemberButton_Click(object sender, EventArgs e)
        {
            if(ValidateForm())
            {
                PersonModel p = new PersonModel();

                p.FirstName = firstNameValue.Text;
                p.LastName = lastNameValue.Text;
                p.EmailAddress = emailValue.Text;
                p.CellphoneNumber = cellphoneValue.Text;

                p = GlobalConfig.Connection.CreatePerson(p);

                //wrzucanie do dropBoxa bezpośrednio po stworzeniu członka 
                selectedTeamMembers.Add(p); 
                WireUpLists();



                //czyszczenie 
                firstNameValue.Text = "";
                lastNameValue.Text = "";
                emailValue.Text = "";
                cellphoneValue.Text = "";

            }
            else
            {
                MessageBox.Show("YOU NEED TO FILL IN ALL OF THE FIELDS!!!");
            }
        }

        private bool ValidateForm()
        {
           bool output = true;

            if(firstNameValue.Text.Length == 0)
            {
                //return false;
                output =  false;

            }

            if (lastNameValue.Text.Length == 0)
            {
                
                output = false;
            }

            if (emailValue.Text.Length == 0)
            {
                output = false;
            }

            if (cellphoneValue.Text.Length == 0)
            {
                output = false;
            }

            return output;
           // return true;

        }

        private void cellphoneValue_TextChanged(object sender, EventArgs e)
        {

        }

        private void addMemberButton_Click(object sender, EventArgs e)
        {
            PersonModel p = (PersonModel)selectTeamMemberDropDown.SelectedItem;
            if (p != null)
            {
                avilableTeamMembers.Remove(p); //usuwa z listy dostepnych ale nie z bazy danych 
                selectedTeamMembers.Add(p); //dopisuje do listy ktr później wrzuca do dropBoxa 

                WireUpLists();
            }

        }

        private void removeSelectedMemberButton_Click(object sender, EventArgs e)
        {
            PersonModel p = (PersonModel)teamMembersListBox.SelectedItem;
            if (p != null)
            {
                selectedTeamMembers.Remove(p); //usuwa z dropBoxa
                avilableTeamMembers.Add(p); //dopisuje do listy dostępnych

                WireUpLists();
            }
        }

        private void createTeamButton_Click(object sender, EventArgs e)
        {
            TeamModel t = new TeamModel();
            t.TeamName = teamNameValue.Text;
            t.TeamMembers = selectedTeamMembers; // list of PersonModel

            GlobalConfig.Connection.CreateTeam(t);

            callingForm.TeamComplete(t);

            this.Close();
        }
    }
}
