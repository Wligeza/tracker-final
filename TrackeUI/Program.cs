﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;

namespace TrackeUI
{
    static class Program
    {
        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // inicjalizacja połączenia bazy danych 
             TrackerLibrary.GlobalConfig.InitializeConnections(DatabaseType.Sql); //or Sql //or TextFile
          // Application.Run(new CreateTournamentForm());
           Application.Run(new TournamentDashboardForm());
        }
    }
}
