﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TrackerLibrary;
using TrackerLibrary.Models;

namespace TrackeUI
{
    public partial class CreateTournamentForm : Form, IPrizeRequestor, ITeamRequestor
    {
        List<TeamModel> avilableTeams = GlobalConfig.Connection.GetTeam_All(); 
        List<TeamModel> selectedTeams = new List<TeamModel>();
        List<PrizeModel> selectedPrizes = new List<PrizeModel>();

        public CreateTournamentForm()
        {
            InitializeComponent();

            WireUpLists();
        }

        private void WireUpLists()
        {
            //drop down
            selectTeamDropDown.DataSource = null;
            selectTeamDropDown.DataSource = avilableTeams;
            selectTeamDropDown.DisplayMember = "TeamName";

            //list box
            tournamentPlayersListBox.DataSource = null;
            tournamentPlayersListBox.DataSource = selectedTeams;
            tournamentPlayersListBox.DisplayMember = "TeamName";

            prizesListBox.DataSource = null;
            prizesListBox.DataSource = selectedPrizes;
            prizesListBox.DisplayMember = "PlaceName";


        }


















        //----------------------------------------------------------------------GUZIORY

        //ADD TEAM GUZIOR
        private void addTeamButton_Click(object sender, EventArgs e)
        {
            TeamModel t = (TeamModel)selectTeamDropDown.SelectedItem;
            if(t != null)
            {
                avilableTeams.Remove(t);
                selectedTeams.Add(t);

                WireUpLists();
            }

        }

        //REMOVE PLAYER GUZIOR
        private void deleteSelectedPlayerButton_Click(object sender, EventArgs e)
        {
            TeamModel t = (TeamModel)tournamentPlayersListBox.SelectedItem;
            if (t != null)
            {
                selectedTeams.Remove(t);
                avilableTeams.Add(t);

                WireUpLists();
            }
        }

        //REMOVE PRIZE GUZIOR
        private void removeSelectedPrizeButton_Click(object sender, EventArgs e)
        {
            PrizeModel p = (PrizeModel)prizesListBox.SelectedItem;
            if(p != null)
            {
                selectedPrizes.Remove(p);

                WireUpLists();
            }
        }

        //CREATE PRIZE GUZIOR
        private void createPrizeButton_Click(object sender, EventArgs e)
        {
            //zadzwon do craetePrizeForm 
            //zwróc PrizeModel
            CreatePrizeForm frm = new CreatePrizeForm(this);
            frm.Show();
        }

        public void PrizeComplete(PrizeModel model)
        {
            selectedPrizes.Add(model);
            WireUpLists();
        }
        
        //CREATE TEAM GUZIOR
        private void createNewTeamLink_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            CreateTeamForm frm = new CreateTeamForm(this);
            frm.Show();
        }

        public void TeamComplete(TeamModel model)
        {
            selectedTeams.Add(model);
            WireUpLists();
        }
        



        private void createTournamenButton_Click(object sender, EventArgs e)
        {

            // validate data 
            decimal fee = 0;
            bool feeAcceptable = decimal.TryParse(EntryFeeValue.Text, out fee);


            if(!feeAcceptable)
            {
                MessageBox.Show("YOU NEED TO ENTER A VALID ENTRY FEE", "INVALID FEE!!!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //create tournament model 
            TournamenModel tm = new TournamenModel();

            tm.TournamentName = tournamentNameValue.Text;
            tm.EntryFee = fee;
            tm.Prizes = selectedPrizes;
            tm.EnteredTeams = selectedTeams;

            // Wire our matchups
            TournamentLogic.CreateRounds(tm);


            /// ranodomowo wybierasz drużyny 
            /// jeżeli ilosc drużyn jest nie wygodna - dodaj byes; potrzebujesz 2^n drużyn 
            /// stwórz pierwszą rundę matchupów
            /// stwórz resztę tych drużyn   


            // Create Tournament entry
            // Create all of the prizes entries
            // Create all of team entries
            GlobalConfig.Connection.CreateTournament(tm);
        }
    }
}
